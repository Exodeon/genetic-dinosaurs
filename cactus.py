import pygame
import os
from random import choice

from constants import *

images = [pygame.transform.scale(pygame.image.load(os.path.join("assets/CACTUS" + str(i) + ".png")),
                                                  (IMAGE_SIZE//2, IMAGE_SIZE)) for i in range(1, 5)]


class Cactus:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.width = IMAGE_SIZE // 2
        self.height = IMAGE_SIZE
        self.image = choice(images)

    def move(self, scroll_speed):
        self.x -= scroll_speed

    def draw(self, win):
        win.blit(self.image, (self.x - self.width // 2, self.y - self.height // 2))
