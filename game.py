import pygame
import os
from random import random

from dinosaur import Dinosaur
from cactus import Cactus
from large_cactus import LargeCactus
from constants import *

floor_width = 30
floor_image = pygame.transform.scale(pygame.image.load(os.path.join("assets/floor-1.png")),
                                     (WIDTH, floor_width))


class Game:
    def __init__(self, dinosaurs):
        self.width = WIDTH
        self.height = HEIGHT
        self.image_size = IMAGE_SIZE
        pygame.init()
        self.win = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption("Genetic Dinosaurs")
        self.background1_x = 0
        self.background2_x = self.width
        self.dinosaurs = dinosaurs

        self.cactus_list = [Cactus(self.width, self.height // 2)]
        self.cactus_spawn_rate = 0.02
        self.scroll_speed = SCROLL_SPEED
        self.score = 0
        self.font = pygame.font.Font("assets/font/pixelmix.ttf", 35)
        self.generation_count = 1

    def reset(self, dinosaurs):
        self.dinosaurs = dinosaurs
        self.cactus_list = [Cactus(self.width, self.height // 2)]
        self.score = 0
        self.generation_count += 1
        self.scroll_speed = SCROLL_SPEED

    def draw(self):
        self.win.fill(BACKGROUND_COLOR)
        # background 1
        self.win.blit(floor_image, (self.background1_x, self.height // 2 + self.image_size//2 - 10))
        # background 2
        self.win.blit(floor_image, (self.background2_x, self.height // 2 + self.image_size//2 - 10))

        for cactus in self.cactus_list:
            cactus.draw(self.win)
        for dinosaur in [dino for dino in self.dinosaurs if dino.alive]:
            dinosaur.draw(self.win)

        score_text = self.font.render("Score : " + str(self.score), True, (0, 0, 0))
        self.win.blit(score_text, (10, 10))
        dinosaurs_left_text = self.font.render("Remaining dinosaurs : " +
                                               str(len([dino for dino in self.dinosaurs if dino.alive])), True, (0, 0, 0))
        self.win.blit(dinosaurs_left_text, (10, 60))
        generation_text = self.font.render("Generation " + str(self.generation_count), True, (0, 0, 0))
        self.win.blit(generation_text, (10, 110))

    def play(self):
        pygame.display.update()
        clock = pygame.time.Clock()

        run = True
        while run:
            clock.tick(FPS)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                    return "end"

            keys = pygame.key.get_pressed()
            for dinosaur in [dino for dino in self.dinosaurs if dino.alive]:
                #if keys[pygame.K_SPACE]:
                #    dinosaur.is_jumping = True
                #else:
                #    if dinosaur.is_jumping and dinosaur.jump_count > 0:
                #        dinosaur.jump_count = -dinosaur.jump_count - 1

                dinosaur.move(self.cactus_list)
                if dinosaur.collide(self.cactus_list):
                    dinosaur.score = self.score
                    dinosaur.alive = False
            self.score += 1

            if self.score % 300 == 0:
                self.scroll_speed += 1

            if max(self.cactus_list, key=lambda cactus: cactus.x).x < WIDTH // 4 * 3 and random() < self.cactus_spawn_rate:
                rand_spawn = random()
                if rand_spawn < 0.1:
                    self.cactus_list.append(LargeCactus(self.width, self.height // 2))
                elif rand_spawn < 0.2:
                    cactus = Cactus(self.width, self.height // 2)
                    self.cactus_list.append(cactus)
                    self.cactus_list.append(Cactus(self.width + cactus.width, self.height // 2))
                elif rand_spawn < 0.3:
                    cactus = Cactus(self.width, self.height // 2)
                    self.cactus_list.append(cactus)
                    self.cactus_list.append(Cactus(self.width + cactus.width * 2, self.height // 2))
                elif rand_spawn < 0.4:
                    cactus = Cactus(self.width, self.height // 2)
                    self.cactus_list.append(cactus)
                    self.cactus_list.append(Cactus(self.width + cactus.width * 4, self.height // 2))
                else:
                    self.cactus_list.append(Cactus(self.width, self.height // 2))

            for cactus in self.cactus_list:
                cactus.move(self.scroll_speed)
                if cactus.y <= 0:
                    self.cactus_list.remove(cactus)

            if len([dino for dino in self.dinosaurs if dino.alive]) <= 0:
                run = False

            self.background1_x -= self.scroll_speed
            self.background2_x -= self.scroll_speed
            if self.background1_x < - self.width:
                self.background1_x = self.width
            if self.background2_x < - self.width:
                self.background2_x = self.width

            self.draw()
            pygame.display.update()


if __name__ == "__main__":
    dinosaur_number = 25
    game = Game([Dinosaur() for x in range(dinosaur_number)])
    game.play()
