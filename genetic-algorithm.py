import random
import pygame
from dinosaur import Dinosaur
from game import Game


class GeneticAlgorithm:

    def __init__(self, population_size, mutation_rate):
        self.population_size = population_size
        self.mutation_rate = mutation_rate
        self.genes_range = [[1, 15], [1, 200]]

    def cross_over(self, parent_a, parent_b):
        midpoint = len(self.genes_range) // 2
        return Dinosaur(parent_a.genes[:midpoint] + parent_b.genes[midpoint:])

    def mutate(self, individual):
        gene_index = random.randrange(len(individual.genes))
        individual.genes[gene_index] = random.randrange(self.genes_range[gene_index][0], self.genes_range[gene_index][1])
        return individual

    def generate_population(self, population_size):
        population = []
        for i in range(population_size):
            genes = []
            for j in range(len(self.genes_range)):
                genes.append(random.randrange(self.genes_range[j][0], self.genes_range[j][1]))
            population.append(Dinosaur(genes))
        return population

    def run(self):
        population = self.generate_population(self.population_size)
        iterations_count = 0
        game = Game(population)
        exit_value = game.play()

        while exit_value != "end":
            new_population = []
            for i in range(self.population_size):
                # we choose 2 parents with weights equals to scores
                parents = random.choices(population, weights=map(lambda x: x.score, population), k=2)
                child = self.cross_over(parents[0], parents[1])
                if random.random() < self.mutation_rate:
                    child = self.mutate(child)
                new_population.append(child)
            population = new_population
            iterations_count += 1
            game.reset(population)
            exit_value = game.play()

pygame.quit()

if __name__ == "__main__":
    gen_alg = GeneticAlgorithm(population_size=25, mutation_rate=0.1)
    gen_alg.run()
