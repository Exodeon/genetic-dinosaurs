import pygame
import os

from constants import *
from cactus import Cactus

image = pygame.transform.scale(pygame.image.load(os.path.join("assets/CACTUS5.png")), (IMAGE_SIZE, IMAGE_SIZE))

class LargeCactus(Cactus):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.width = IMAGE_SIZE
        self.image = image
